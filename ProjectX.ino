int redButton = 11;
int greenButton = 6;
int blueButton = 5;
int redPin = 9;
int greenPin = 3;
int bluePin = 10;
unsigned long previousMillis;
long interval;
bool i = 1;
bool lightOff = 0;

void setup()
{
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT); 
  pinMode(redButton, INPUT);
  pinMode(greenButton, INPUT);
  pinMode(blueButton, INPUT);
  Serial.begin(9600);
}

void loop()
{
  unsigned long currentMillis = millis();
  if(currentMillis - previousMillis > interval && i == 0){
    Serial.println("Millis function");
    i = 1;
    lightOff = 1;
    setColour(0, 0, 0);
  }
  if (digitalRead(blueButton) == HIGH && i == 0){
      setColour(255, 0, 0);
      delay(3000);
      setColour(0, 0, 0);
      i = 1;
  }else if (digitalRead(blueButton) == HIGH && lightOff == 1){
      setColour(0, 0, 120);
      delay(3000);
      setColour(0, 0, 0);
  }
  if (digitalRead(redButton) == HIGH && i == 0){
      setColour(0, 0, 120);
      delay(3000);
      setColour(0, 0, 0);
      i = 1;
  } else if (digitalRead(redButton) == HIGH && lightOff == 1){
      setColour(255, 0, 0);
      delay(3000);
      setColour(0, 0, 0);
  }
  if (digitalRead(greenButton) == HIGH){
  Serial.println("Green Button Pressed");
  setColour(0, 60, 0);
  i = 0;
  previousMillis = millis();
  interval = random(3000, 7000);
  Serial.println(interval);
  lightOff = 0;
  }
}

void setColour(int red, int green, int blue)
{
  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);  
}
